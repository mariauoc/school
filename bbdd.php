<?php
// Fichero donde almacenar las funciones php
// relacionadas con la base de datos

// Función que modifica la nota de un proyecto
function updateMark($idproject, $mark) {
    $c = conectar();
    $update = "update project set mark=$mark where idproject=$idproject";
    if (mysqli_query($c, $update) === false) {
        $resultado = mysqli_error($c);
    } else {
        $resultado = "ok";
    }
    desconectar($c);
    return $resultado;
}

// Función que devuelve id y nombre de los
// proyectos que no tienen nota
function selectProjectWithoutMark() {
    $c = conectar();
    $select = "select idproject, project.name from project where mark is null;";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que devuelve código, nombre y apellidos 
// de todos los alumnos
function selectCodeNameAlumnos() {
    $c = conectar();
    $select = "select code, name, surname from student";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que trae todos los datos de los alumnos
function selectAllAlumno() {
    $c = conectar();
    $select = "select * from student";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función para comprar si existe un alumno
function existeAlumno($codigo) {
    // conectamos bbdd
    $c = conectar();
    // Definimos la consulta
    $select = "select * from student where code=$codigo";
    // Ejecutamos select recogiendo resultado
    $resultado = mysqli_query($c, $select);
    // Miramos si el resultado tiene 1 fila
    if (mysqli_num_rows($resultado) == 1) {
        $existe = true;
    } else {
        $existe = false;
    }
    // desconectamos
    desconectar($c);
    // devolvemos existe
    return $existe;
}

// Función para insertar un proyecto
function insertarProyecto($nombre, $fecha, $alumno) {
    $c = conectar();
    $insert = "insert into project (name, date, student) values "
            . "('$nombre', '$fecha', $alumno)";
    if (mysqli_query($c, $insert) === false) {
        $resultado = mysqli_error($c);
    } else {
        $resultado = "ok";
    }
    desconectar($c);
    return $resultado;
}

// Función para insertar un alumno
function insertarAlumno($codigo, $nombre, $apellidos, $edad, $genero) {
    // conectamos con la bbdd
    $c = conectar();
    // Definimos el insert que queremos hacer
    $insert = "insert into student values "
            . "($codigo, '$nombre', '$apellidos', $edad, '$genero')";
    if (mysqli_query($c, $insert) === false) {
        // Ha habido un error al insertar
        // devuelvo el msg de error 
        $resultado = mysqli_error($c);
    } else {
        // la consulta se ejecuta bien
        $resultado = "ok";
    }
    // cerramos la conexión
    desconectar($c);
    // devolvermos el resultado
    return $resultado;
}



// Función para conectar con la base de datos
function conectar() {
    $conexion = mysqli_connect("localhost", "root", "root", "school");
    // Si ha habido error, finalizo la aplicación
    if (!$conexion) {
        die("Error en la conexión");
    }
    // si todo ha ido ok, devolvemos la conexión
    // para poderla utilizar en el resto de funciones
    return $conexion;
}

// Función para desconectar, recibe una conexión y la cierra
function desconectar($conexion) {
    mysqli_close($conexion);
}

