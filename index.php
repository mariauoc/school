<!DOCTYPE html>
<!--
Main Menu
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
    </head>
    <body>
        <h1>School Web App</h1>
        <p><a href="registroAlumno.php">Registrar alumno</a></p>
        <p><a href="verAlumnos.php">Ver datos de alumnos</a></p>
        <p><a href="registroProyecto.php">Registrar proyecto</a></p>
        <p><a href="modificarProyecto.php">Calificar proyecto</a></p>
        <p><a href="borrarAlumno.php">Borrar proyecto</a></p>
    </body>
</html>
