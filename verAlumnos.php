<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Datos Alumnos</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        // llamamos a la función de la consulta
        // y recogemos la matriz con el resultado
        $alumnos = selectAllAlumno();
        // Abrimos tabla en html
        ?>
        <table>
            <tr><th>Codigo</th><th>Nombre</th><th>Apellidos</th><th>Edad</th><th>Genero</th></tr>
            <?php
            // Ahora con un bucle, vamos a ir extrayendo
            // fila a fila para ir mostrando los datos
            // en una tabla
            // mientras haya filas (mysqli_fetch_assoc devuelve null cuando no hay más filas)
            while ($fila = mysqli_fetch_assoc($alumnos)) {
                echo "<tr>";
                echo "<td>".$fila["code"]."</td>";
                echo "<td>".$fila["name"]."</td>";
                echo "<td>".$fila["surname"]."</td>";
                echo "<td>".$fila["age"]."</td>";
                echo "<td>".$fila["gender"]."</td>";
                echo "</tr>";
            }
            ?>
        </table>
        <?php
        ?>
    </body>
</html>
