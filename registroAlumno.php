<!DOCTYPE html>
<!--
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>New Student</title>
    </head>
    <body>
        <form method="POST">
            <p>Codigo: <input type="number" name="codigo" min="0" required></p>
            <p>Nombre: <input type="text" name="nombre" maxlength="25" required></p>
            <p>Apellidos: <input type="text" name="apellidos" maxlength="45" required></p>
            <p>Edad: <input type="number" name="edad" min="15" max="120" required></p>
            <p>Genero: 
                <input type="radio" name="genero" value="Mujer" checked> Mujer
                <input type="radio" name="genero" value="Hombre"> Hombre
            </p>
            <p><input type="submit" name="boton" value="Alta"></p>
        </form>

        <?php
        // Incluimos el fichero de funciones
        require_once("bbdd.php");
        // Comprobamos que se ha rellenado el formulario (y pulsado botón)
        if (isset($_POST["boton"])) {
            // Recogemos datos del form
            $codigo = $_POST["codigo"];
            if (existeAlumno($codigo)) { // if (existeAlumno($codigo)==true
                echo "Ya existe un alumno con ese código<br>";
            } else {
                $nombre = $_POST["nombre"];
                $apellidos = $_POST["apellidos"];
                $edad = $_POST["edad"];
                $genero = $_POST["genero"];
                // Una vez comprobado registramos el alumno en la bbdd
                $resultado = insertarAlumno($codigo, $nombre, $apellidos, $edad, $genero);
                if ($resultado == "ok") {
                    echo "Alumno registrado en la bbdd<br>";
                } else {
                    echo "ERROR: $resultado<br>";
                }
            }
        }
        ?>
    </body>
</html>
