<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Poner nota</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        // hacemos la consulta de proyectos por calificar
        $resultado = selectProjectWithoutMark();
        // comprobar si hay algo por calificar
        if (mysqli_num_rows($resultado) == 0) {
            echo "<p>No hay proyectos por calificar</p>";
        } else {
            // Rellenamos select del form con los datos
            ?>
            <form method="POST">
                <p>Proyecto: <select name="proyecto" required>
                        <?php
                        while ($fila = mysqli_fetch_assoc($resultado)) {
                            echo "<option value='" . $fila["idproject"] . "'>";
                            echo $fila["idproject"] . " - " . $fila["name"];
                            echo "</option>";
                        }
                        ?>
                    </select></p>
                <p>Nota: <input type="number" name="nota" min="0" max="10" required></p>
                <input type="submit" name="boton" value="Calificar">
            </form>
            <?php
            if (isset($_POST["boton"])) {
                $idproject = $_POST["proyecto"];
                $nota = $_POST["nota"];
                $resultado = updateMark($idproject, $nota);
                if ($resultado == "ok") {
                    echo "<p>Proyecto calificado</p>";
                } else {
                    echo "ERROR: $resultado<br>";
                }
            }
        }
        ?>
    </body>
</html>
