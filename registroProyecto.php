<html>
    <head>
        <meta charset="UTF-8">
        <title>Nuevo Proyecto</title>
    </head>
    <body>
        <h2>Registro Proyecto</h2>
        <form method="POST">
            <p>Nombre: <input type="text" name="nombre" required></p>
            <p>Fecha: <input type="date" name="fecha" required></p>
            <p>Alumno <select name="alumno" required>
                <?php
                require_once 'bbdd.php';
                $datosAlumnos = selectCodeNameAlumnos();
                // bucle para ir rellenando el select
                while ($fila= mysqli_fetch_assoc($datosAlumnos)) {
                    echo "<option value='".$fila["code"]."'>";
                    echo $fila["code"]." - ".$fila["name"]." ". $fila["surname"];
                    echo "</option>";
                }
                ?>
                </select></p>
                <input type="submit" name="boton" value="Registrar">
        </form>
        <?php
        if (isset($_POST["boton"])) {
            // Recogemos valores del form
            $nombre = $_POST["nombre"];
            $fecha = $_POST["fecha"];
            $alumno = $_POST["alumno"];
            $resultado = insertarProyecto($nombre, $fecha, $alumno);
            if ($resultado == "ok") {
                echo "Proyecto registrado<br>";
            } else {
                echo "ERROR: $resultado<br>";
            }
        }
        ?>
    </body>
</html>
